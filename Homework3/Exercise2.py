import numpy as np

n = int(input("Enter N: "))
a = np.random.uniform(0, 10, size=(n, n, n))
print(a, end="\n\n")

print("Find max, min, sum by depth: \n")
print(a.max(axis=0), end="\n\n")
print(a.min(axis=0), end="\n\n")
print(a.sum(axis=0), end="\n\n")

print("Find max, min, sum by height: \n")
print(a.max(axis=1), end="\n\n")
print(a.min(axis=1), end="\n\n")
print(a.sum(axis=1), end="\n\n")

print("Find max, min, sum by width: \n")
print(a.max(axis=2), end="\n\n")
print(a.min(axis=2), end="\n\n")
print(a.sum(axis=2))
