import numpy as np

iris = np.loadtxt("iris.txt", dtype={'names': ('sepal length', 'sepal width', 'petal length', 'petal width', 'label'),
'formats': (np.float, np.float, np.float, np.float, '|S15')}, delimiter=',')

result_list = []
for item in iris:
    item_list = list(item)
    for index, val in enumerate(item_list):
        if "setosa" in str(val):
            item_list[index] = 1
        elif "versicolor" in str(val):
            item_list[index] = 2
        elif "virginica" in str(val):
            item_list[index] = 3
    result_list.append(item_list)

result_array = np.asarray(result_list)
print(result_array)
print(np.cov(result_array))
