import pandas as pd
import numpy as np
import random

id_list = ["id" + str(x) for x in range(1, 21)]
s1 = pd.DataFrame(random.sample(id_list, 10))
s2 = pd.DataFrame(random.sample(id_list, 10))
s3 = pd.DataFrame(np.random.random_integers(-100, 100, size=10))
s4 = pd.DataFrame(np.random.random_integers(-100, 100, size=10))
print(s1)
print(s2)
print(s3)
print(s4)

df1 = pd.concat([s1, s3], axis=1)
df1.columns=["key", "data1"]
df2 = pd.concat([s2, s4], axis=1)
df2.columns=["key", "data2"]
print(df1)
print(df2)
print(pd.merge(df1, df2, on="key"))
