import pandas as pd
import numpy as np

df = pd.DataFrame()
df['month'] = pd.date_range(start='20180131', periods=50, freq='M')
df['sale'] = np.random.randint(11, 30, size=50)
df['year'] = df.month.map(lambda x: x.year)
print(df)
print(df.groupby(df.year).sum())

# if don't want to change the dataframe structure
#sum = df.groupby(df.month.dt.year).sum()

#Optional part
new_df = pd.DataFrame()
new_df['month'] = pd.date_range(start='20180131', periods=200, freq='D')
new_df['sale'] = np.random.randint(11, 30, size=200)
print(new_df.groupby(new_df.month.dt.month).sum())